"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const pg_1 = __importDefault(require("pg"));
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
// Construct the path to the .pem file
const caPath = path_1.default.join(__dirname, 'DigiCertGlobalRootCA.crt.pem');
// Read the file
const ca = fs_1.default.readFileSync(caPath).toString();
const pool = new pg_1.default.Pool({
    user: 'samSqlServerAdmin',
    host: 'samtodolist.postgres.database.azure.com',
    database: 'postgres',
    password: 'PostgreSQLPW#',
    port: 5432,
    ssl: {
        rejectUnauthorized: true,
        ca: ca
    }
});
exports.default = pool;
