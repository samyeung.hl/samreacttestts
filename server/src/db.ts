import pg from 'pg';
import fs from 'fs';
import path from 'path';


// Construct the path to the .pem file
const caPath = path.join(__dirname, 'DigiCertGlobalRootCA.crt.pem');

// Read the file
const ca = fs.readFileSync(caPath).toString();

const pool = new pg.Pool({
  user: 'samSqlServerAdmin',
  host: 'samtodolist.postgres.database.azure.com',
  database: 'postgres',
  password: 'PostgreSQLPW#',
  port: 5432,
  ssl: {
      rejectUnauthorized: true,
      ca: ca
    }
});

export default pool;