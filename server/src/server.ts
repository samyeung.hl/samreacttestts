import express from 'express';
import path from 'path';
import pool from './db'

const app = express();
app.use(express.json());
const port = process.env.PORT || 3001; // Different port than React app 
app.use(express.static(path.join(__dirname, '../../build')));

app.get('/api/tasks', async (req, res) => {
    try {
      const { rows } = await pool.query('select id ,name from public.to_do_list ORDER BY id DESC;');
      res.json(rows);
    } catch (err: any) {
      console.error(err.message);
      res.status(500).send('Server error');
    }
});

app.get('/api/task/:taskId', async (req, res) => {
    const taskId = req.params.taskId;
    try {
      const { rows } = await pool.query('SELECT id, name FROM public.to_do_list WHERE id = $1', [taskId]);
      if (rows.length > 0) {
        res.json(rows[0]);
      } else {
        res.status(404).send('Task not found');
      }
    } catch (err: any) {
      console.error(err.message);
      res.status(500).send('Server error');
    }
});

app.post('/api/add-task', async (req, res) => {
    const { taskName } = req.body;
    try {
      const result = await pool.query('INSERT INTO public.to_do_list(name) VALUES($1) RETURNING *', [taskName]);
      res.status(201).json(result.rows[0]);
    } catch (err: any) {
      res.status(500).json({ error: err.message });
    }
});

app.put('/api/task/:taskId', async (req, res) => {
    const taskId = req.params.taskId;
    const taskName = req.body.name;
    if (!taskName) {
      return res.status(400).send('Task name is required');
    }
  
    try {
      const { rows } = await pool.query(
        'UPDATE public.to_do_list SET name = $1 WHERE id = $2 RETURNING *',
        [taskName, taskId]
      );
  
      if (rows.length > 0) {
        res.json(rows[0]);
      } else {
        res.status(404).send('Task not found');
      }
    } catch (err: any) {
      console.error(err.message);
      res.status(500).send('Server error');
    }
  });
  
  app.delete('/api/task/:taskId', async (req, res) => {
    const taskId = req.params.taskId;
  
    try {
      const result: any = await pool.query('DELETE FROM public.to_do_list WHERE id = $1 RETURNING *', [taskId]);
      
      if (result.rowCount > 0) {
        res.json({ message: 'Task deleted successfully' });
      } else {
        res.status(404).send('Task not found');
      }
    } catch (err: any) {
      console.error(err.message);
      res.status(500).send('Server error');
    }
});

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../../build', 'index.html'));
});


app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});