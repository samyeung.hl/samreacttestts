"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const path_1 = __importDefault(require("path"));
const db_1 = __importDefault(require("./db"));
const app = (0, express_1.default)();
app.use(express_1.default.json());
const port = process.env.PORT || 3001; // Different port than React app 
app.use(express_1.default.static(path_1.default.join(__dirname, '../../build')));
app.get('/api/tasks', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { rows } = yield db_1.default.query('select id ,name from public.to_do_list ORDER BY id DESC;');
        res.json(rows);
    }
    catch (err) {
        console.error(err.message);
        res.status(500).send('Server error');
    }
}));
app.get('/api/task/:taskId', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const taskId = req.params.taskId;
    try {
        const { rows } = yield db_1.default.query('SELECT id, name FROM public.to_do_list WHERE id = $1', [taskId]);
        if (rows.length > 0) {
            res.json(rows[0]);
        }
        else {
            res.status(404).send('Task not found');
        }
    }
    catch (err) {
        console.error(err.message);
        res.status(500).send('Server error');
    }
}));
app.post('/api/add-task', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { taskName } = req.body;
    try {
        const result = yield db_1.default.query('INSERT INTO public.to_do_list(name) VALUES($1) RETURNING *', [taskName]);
        res.status(201).json(result.rows[0]);
    }
    catch (err) {
        res.status(500).json({ error: err.message });
    }
}));
app.put('/api/task/:taskId', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const taskId = req.params.taskId;
    const taskName = req.body.name;
    if (!taskName) {
        return res.status(400).send('Task name is required');
    }
    try {
        const { rows } = yield db_1.default.query('UPDATE public.to_do_list SET name = $1 WHERE id = $2 RETURNING *', [taskName, taskId]);
        if (rows.length > 0) {
            res.json(rows[0]);
        }
        else {
            res.status(404).send('Task not found');
        }
    }
    catch (err) {
        console.error(err.message);
        res.status(500).send('Server error');
    }
}));
app.delete('/api/task/:taskId', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const taskId = req.params.taskId;
    try {
        const result = yield db_1.default.query('DELETE FROM public.to_do_list WHERE id = $1 RETURNING *', [taskId]);
        if (result.rowCount > 0) {
            res.json({ message: 'Task deleted successfully' });
        }
        else {
            res.status(404).send('Task not found');
        }
    }
    catch (err) {
        console.error(err.message);
        res.status(500).send('Server error');
    }
}));
app.get('*', (req, res) => {
    res.sendFile(path_1.default.join(__dirname, '../../build', 'index.html'));
});
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
