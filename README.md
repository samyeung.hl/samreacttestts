## public git
https://gitlab.com/samyeung.hl/samreacttestts

## node version
v18.19.0

## git clone
https://gitlab.com/samyeung.hl/samreacttestts.git

## install libraries after git clone
npm install

## build and run locally
npm run build-server
npm run build
npm start

## test case
![1. add task](image.png)
![Alt text](image-1.png)
![Alt text](image-2.png)
![Alt text](image-3.png)
![Alt text](image-4.png)
![Alt text](image-5.png)